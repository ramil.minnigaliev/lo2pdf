# lo2pdf

Converter of any supported by LibreOffice Writer document to PDF (odt2pdf, doc2pdf, docx2pdf, etc).

## Usage
### Python Example
```python
def get_pdf(path):
    with open(path, 'rb') as f:
        r = requests.post('http://lo2pdf:6000', files={
            'upload_file': f
        }, stream=True)

        if r.status_code == 200:
            r.raw.decode_content = True
            return r.raw
```
