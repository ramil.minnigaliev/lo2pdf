FROM python:3.8

LABEL maintainer="minnigaliev-r@yandex.ru"

RUN echo deb http://deb.debian.org/debian/ buster main non-free contrib >> /etc/apt/sources.list \
    && sed -i 's/deb.debian.org/mirror.yandex.ru/g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y \
    libreoffice-writer \
    openjdk-11-jre-headless \
    ttf-mscorefonts-installer \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir /app

COPY requirements/requirements.txt /app

RUN pip install -r /app/requirements.txt

COPY main.py /app/main.py

ENV PORT 6000

EXPOSE 6000

WORKDIR /app

ENTRYPOINT ["python", "/app/main.py"]
