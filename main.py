from aiohttp import web
import os
import subprocess


async def lo2pdf(request):
    file = 'file'
    with open(file, 'wb') as f:
        reader = await request.multipart()
        doc = await reader.next()

        while True:
            chunk = await doc.read_chunk()
            if not chunk:
                break
            f.write(chunk)

    subprocess.run([
        'soffice',
        '--headless',
        '--convert-to',
        'pdf',
        file
    ])

    os.remove(file)

    pdf = f'{file}.pdf'
    response = web.StreamResponse(
        status=200,
        reason='OK',
    )
    response.content_type = 'application/pdf'

    await response.prepare(request)

    try:
        with open(pdf, 'rb') as f:
            while True:
                chunk = f.read(8192)
                if not chunk:
                    break
                await response.write(chunk)
    except Exception as e:
        print(e)
        response = web.Response(status=400)

    os.remove(pdf)

    return response

if __name__ == '__main__':
    app = web.Application()
    app.router.add_post('/', lo2pdf)

    web.run_app(app, port=int(os.getenv('PORT', '6000')))
